package de.dpunkt.myaktion.model;

import java.util.Date;

/**
 * @author Julian
 *
 */
public class DateEntity {

    private Date createdAt;

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
    
}
