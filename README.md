
# NoSQL-Datenbank für My-Aktion
## Worum geht es in diesem Projekt?


Die Aufgabe dieses Projekts ist es die relationale H2-Datenbank des Projekts my-aktion durch die NoSQL-Datenbank MongoDB zu ersetzen, sowie dem Anpassen von my-aktion an die neue Datenbank.  
  
Zur Umsetzung wurde das Mapping-Framework Hibernate OGM genutzt. Hibernate OGM mappt die Entitäten der Anwendung nicht wie Hibernate ORM, an eine relationale Datenbank, sondern an eine NoSQL-Datenbank. Im Falle des Projekts also an die MongoDB-Datenbank.  
  
Hibernate OGM ist dazu in der Lage JPQL-Queries zu übersetzen in die Sprache der jeweils angebundenen Datenbank. Der Programmierer muss also die Abfragesprache der genutzten Datenbank nicht zwangsläufig kennen. Auch die von der Anwendung genutzten JPA Annotationen wie zum Beispiel *NotNull* oder *ManyToOne* werden unterstützt, also müssen auch hier keine Anpassungen vorgenommen werden.                        

## Aufgetretene Probleme
### Aggregatfunktionen in Hibernate OGM

Da Hibernate OGM ein laufender Prozess ist können bislang nur manche Sprachkonstrukte von JPQL in andere Abfragesprachen übersetzt werden. Was genau übersetzt wird, unterscheidet sich je nach genutzter Datenbank. Die Version für MongoDB-Datenbanken unterstützt die folgenden Konstrukte von JPQL:

-   einfache Vergleiche mit "<", "<=", "=", ">=" und ">"
-   `IS NULL` und `IS NOT NULL`
-   Die booleschen Operatoren`AND`, `OR` und `NOT`
-   `LIKE`, `IN` und `BETWEEN`
-   `ORDER BY`

Im Projekt my-aktion wird an einer Stelle die SQL-Aggregatfunktion SUM genutzt, in der Dokumentation werden diese nicht bei unterstützten Konstrukten aufgezählt. Allerdings scheint dennoch zumindest eine teilweise Unterstützung für HQL zu bestehen. Näheres weiter unten. Die restlichen Abfragen konnten problemlos weiter genutzt werden ohne spezielle Anpassungen vornehmen zu müssen.  
  
Der unten dargestellte Code zeigt die Methode, in welcher die Abfrage mit der Summenfunktion aufgerufen wird. Der einzige Unterschied zwischen diesem und den anderen Methoden in der Klasse ist der, dass anstatt der JPA-API die Hibernate-Native-API genutzt wird und damit HQL anstatt JPQL.
 
	@RolesAllowed("Organizer")
	@Stateless
	public class CampaignServiceBean implements CampaignService {    
	    // ----------------------------------------------------------------------------------------------
	    
	    ...
	
	    private Double getAmountDonatedSoFar(Campaign campaign) {        
		Session session = entityManager.unwrap(Session.class);

	        Double result = session.createNamedQuery(Campaign.getAmountDonatedSoFar, Double.class)
	                .setParameter("campaign", campaign).uniqueResult();
	        if (result == null)
	            result = 0d;

	        return result;
	    }
	    
        ...
    
	    // ----------------------------------------------------------------------------------------------
    }



Da im Code nur auf das Session-Interface zugegriffen wird und nicht direkt auf die OgmSession, sollte die Methode auch mit einer Hibernate-ORM-Lösung lauffähig sein, müsste also nicht extra angepasst werden. Der Nachteil dieser Methode ist allerdings dass die reine Nutzung von JPA damit entfällt und somit nicht einfach auf eine andere Implementierung wie zum Beispiel EclipseLink umgestiegen werden könnte.  
  
Die Lösung wurde auf der folgenden Webseite gefunden https://www.gregoriopalama.com/mongodb-on-wildfly-using-hibernate-ogm/  
  
Nach weiterer Recherche warum das genannte funktioniert, obwohl gegenteiliges in der Dokumentation behauptet wird, konnten folgende JIRA-Einträge von Hibernate OGM gefunden werden.  
  
- https://hibernate.atlassian.net/browse/OGM-534  
- https://in.relation.to/2018/12/18/hibernate-ogm-5-4-1-Final-released/  
- https://hibernate.atlassian.net/browse/OGM-1530  
  
Beide sind als erledigt markiert. Im ersten wird Support für die SQL-Aggregatfunktion Count hinzugefügt. Diese Änderung ist bereits veröffentlicht worden in der Version 5.4.1 (siehe zweiter Link) welche die aktuelle Veröffentlichung darstellt und die im Projekt genutzte.  
  
Des Weiteren ist auf der ersten Seite die Aufgabe verlinkt die weiteren Aggregatfunktionen zu implementieren. Die verlinkte Aufgabe ist der dritte Link. Wenn man sich den Github Link für den dazugehörigen Pull Request ansieht und dessen Kommentare und Commits sieht es so aus als ob die Änderungen bereits veröffentlicht wurden die Dokumentation diesbezüglich aber nicht aktualisiert worden ist.  
  
Eine mögliche Alternative wäre es den NamedQuery mit der *criteria-only find syntax* oder der *MongoDB CLI syntax* und einem NativeNamedQuery zu schreiben. Da ein einfaches Wechseln zwischen Hibernate ORM und OGM dadurch erschwert werden würden wurde bewusst darauf verzichtet.  
  
- MongoDB Native Queries: https://docs.jboss.org/hibernate/stable/ogm/reference/en-US/html_single/#ogm-mongodb-queries-native

### Organizer-Dokument

Da die Dokumente in einer MongoDB-Datenbank immer einen eindeutigen Identifier im Feld *_id* haben müssen, wurde in die Organizer-Entität ein Feld id eingefügt welches das email-Feld als Identifier ersetzt. Damit dennoch sichergestellt wird das eine bestimmte E-Mail nur einmal vorkommt in der Tabelle, wurde an das email-Feld die Annotation @Column(unique = true) angehängt (siehe untenstehender Code).

	@Entity
	public class Organizer extends DateEntity {
	    //----------------------------------------------------------------------------------------------

	    public static final String findByEmail = "Organizer.findByEmail";

	    //==============================================================================================
	
	    @GeneratedValue
	    @Id
	    private Long id;
	    
	    @NotNull
	    @Size(min = 3, max = 20, message = "{organizer.firstName.size}")
	    private String firstName;

	    @NotNull
	    @Size(min = 3, max = 30, message = "{organizer.lastName.size}")
	    private String lastName;

	    @Pattern(regexp = ".+@.+", message = "{organizer.email.pattern}")
	    @Column(unique = true)
	    private String email;

	    @NotNull
	    private String password;

	    //----------------------------------------------------------------------------------------------
	    
	    ...
	}

## Wie startet man das Projekt?
### Voraussetzungen
*Anmerkung: Es wurde zum Test ein neu installierter Wildfly-Server genutzt.*
- Eine MongoDB-Datenbank-Instanz muss auf dem Rechner laufen (unter dem Standard Port und localhost).  
- Die Datenbank muss den Namen *my-aktion* haben.  
- Es muss eine Sammlung namens Organizer existieren in welchen die Benutzer der Applikation gespeichert werden.  
- Die in der Anwendung der Vorlesung erstellten Benutzer Martha und Max Mustermann sind in der organizers.json Datei enthalten (Diese befindet sich im Ordner etc des Projekts) und können in die Organizer-Sammlung importiert werden. Hier ist zu beachten dass das Feld _id als ein Int64-Datentyp erstellt wird und nicht als ein Int32-Datentyp. Die Standard-Einstellung ist der Int32-Datentyp. Der Int64-Datentyp entspricht einem Long in Java, da die Entität ebenfalls ein Long enthält wird bei dem Int32-Datentyp eine Exception geworfen. Diese Einstellung kann zum Beispiel mit der Anwendung MongoDBCompass gesetzt werden, mithilfe welcher auf die Datenbank zugegriffen werden kann sowie diese verwaltet werden kann. Hier kann beim einfügen eines neuen Dokuments in eine Sammlung der Datentyp des jeweiligen Felds bestimmt werden.
- Das Projekt MongoDB Login Modul muss fertig konfiguriert sein (siehe https://gitlab.reutlingen-university.de/hornerj/mongo-db-login-module).

### Ausführen
Das Projekt kann auf herkömmlich genutzte Weiße deployt werden also zum Beispiel mit dem Befehl

	mvn clean package wildfly:deploy 

oder auch über die Benutzerschnittstelle von Wildfly die HAL-Management-Konsole.


Ansonsten sind keine besonderen Schritte zu tätigen.

